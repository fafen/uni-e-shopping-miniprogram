import Vue from 'vue';
export default {
	// 为当前模块开启命名空间
	namespaced: true,

	state: { //存放状态
		cart: JSON.parse(uni.getStorageSync('cart') || '[]')
	},
	mutations: {
		addToCart(state, goods) {
			// console.log(state.cart);
			// console.log(state.age);
			const findResult = state.cart.find(item =>
				item.id === goods.id
			)

			if (!findResult) {
				// state.cart.push(goods)
				Vue.set(state,'cart',[goods]);
			} else {
				findResult.count += goods.count
				// findResult.selectStatus = true;
				// findResult.show = false
			}

			this.commit('m_cart/saveToStorage')
			console.log(state.cart);
		},
		// 持久化
		saveToStorage(state) {
			uni.setStorageSync('cart', JSON.stringify(state.cart))
		},
		// 选择商品
		updateGoodsState(state, goods) {
			// console.log(state.cart);
			const findResult = state.cart.find(item => item.id === goods.goods_id)
			// state.cart[0].selectStatus = !state.cart[0].selectStatus
			if (findResult) {
				findResult.selectStatus = !goods.goods_state
				this.commit('m_cart/saveToStorage')
			}
			// console.log(findResult);
		},
		// 更新购物车中商品的数量
		updateGoodsCount(state, goods) {
			// 根据 goods_id 查询购物车中对应商品的信息对象
			const findResult = state.cart.find(item => item.id === goods.goods_id)

			if (findResult) {
				// 更新对应商品的数量
				findResult.count = goods.goods_count
				// 持久化存储到本地
				this.commit('m_cart/saveToStorage')
			}
		},
		// 废弃，SwipeAction相关
		updateSwipeActionShow(state, goods_id) {
			const findResult = state.cart.find(item => item.id === goods_id)
			const findResultIndex = state.cart.findIndex(item => item.id === goods_id)

			// console.log(findResult, findResultIndex);
			if (findResult) {
				findResult.show = true
				state.cart.map((val, idx) => {
					if (findResultIndex != idx) state.cart[idx].show = false;
				})
				// this.commit('saveToStorage')
			}
		},
		// 根据下标删除一条数据
		removeGoodsById(state, index) {
			// state.cart = state.cart.filter(item => item.id !== goods_id)
			state.cart.splice(index, 1);
			this.commit('m_cart/saveToStorage')
		},
		// 更新所有商品的勾选状态
		updateAllGoodsState(state, newState) {
			// 循环更新购物车中每件商品的勾选状态
			state.cart.forEach(item => item.selectStatus = newState)
			// 持久化存储到本地
			this.commit('m_cart/saveToStorage')
		}
	},
	getters: {
		// 统计购物车中商品的总数量
		total(state) {
			let totalCount = 0;
			state.cart.forEach(item => totalCount += item.count)
			return totalCount
		},
		// 勾选的商品的总数量
		checkedCount(state) {
			return state.cart.filter(item => item.selectStatus).reduce((total, item) => total += item.count, 0)
		},
		// 已勾选的商品的总价
		checkedGoodsAmount(state) {
			// 先使用 filter 方法，从购物车中过滤器已勾选的商品
			// 再使用 reduce 方法，将已勾选的商品数量 * 单价之后，进行累加
			// reduce() 的返回值就是已勾选的商品的总价
			// 最后调用 toFixed(2) 方法，保留两位小数
			return state.cart.filter(item => item.selectStatus)
				.reduce((total, item) => total += item.count * item.price, 0)
				.toFixed(2)
		}
	}

}
