export default {
	// 为当前模块开启命名空间
	namespaced: true,

	// 模块的 state 数据
	state: () => ({
		// search 
		historyList: JSON.parse(uni.getStorageSync('History') || '[]')
	}),

	// 模块的 mutations 方法
	mutations: {
		set_history_list(state,history) {
			state.historyList = history
			this.commit('m_search/saveHistoryStorage')
		},
		saveHistoryStorage(state) {
			uni.setStorageSync('History', JSON.stringify(state.historyList))
		},
		clear_history(state) {
			state.historyList = []
			uni.removeStorageSync('History')
		}
	},
	
	actions: {
		set_history({commit,state},history) {
			let list = state.historyList
			// console.log(list);
			if (list.some(item => item.name == history.name)) {
				return
			}
			// console.log(list.some(item => item.name == history.name));
			// console.log(history.name);
			list.unshift(history)
			commit('set_history_list',list)
		},
		clear_history({commit}) {
			commit('clear_history')
		}
	},

	// 模块的 getters 属性
	getters: {},
}
