import Vue from 'vue'
import App from './App'
import store from './store/Store.js'	//1

import uView from 'uview-ui';
Vue.use(uView);

Vue.config.productionTip = false
Vue.prototype.$store = store //2

App.mpType = 'app'

// 把 store 对象提供给 “store” 选项，这可以把 store 的实例注入所有的子组件
const app = new Vue({
	store,
    ...App
})
app.$mount()
