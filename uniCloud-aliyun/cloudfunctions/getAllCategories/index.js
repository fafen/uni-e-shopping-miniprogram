'use strict';
const db = uniCloud.database();
const dbCmd = db.command
const $ = db.command.aggregate
exports.main = async (event, context) => {

	let {id} = event
	console.log(typeof id);
	let result = await db.collection('category').aggregate()
		.lookup({
			from: 'image',
			localField: 'topic_img_id',
			foreignField: 'id',
			as: 'image'
		})
		.match({
			id: dbCmd.in(typeof id === "object" ? id : [id])
		})
		.end()

	//返回数据给客户端
	return {
		code: 200,
		msg: '数据获取成功',
		data: result.data,
	}
};
