'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {

	let result = await db.collection('product')
		.aggregate()
		.lookup({
			from: "image",
			localField: "img_id",
			foreignField: "id",
			as: "imageUrl"
		})
		.addFields({
			url: '$imageUrl.url'
		})
		.project({
			imageUrl: false
		})
		.unwind(
			'$url'
		)
		.sort({
			create_time: 1
		})
		.limit(6)
		.end()



	// .orderBy("create_time","desc").limit(6).get()

	//返回数据给客户端
	return {
		code: 200,
		msg: '数据获取成功',
		data: result.data
	}
};
