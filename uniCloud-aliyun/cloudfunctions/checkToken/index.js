'use strict';
const uniID = require('uni-id')
exports.main = async (event, context) => {
	
	// uniID.checkToken(String token, Object checkTokenOptions)
	let result = await uniID.checkToken(event.token,{
		// needPermission:true,
		// needUserInfo:true
	})
	
	//返回数据给客户端
	return result
};
