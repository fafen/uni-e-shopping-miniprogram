'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {

	const list = await db.collection('product')
		.aggregate()
		.lookup({
			from: "image",
			localField: "img_id",
			foreignField: "id",
			as: "imageUrl"
		})
		.addFields({
			url: '$imageUrl.url'
		})
		.project({
			imageUrl: false
		})
		.unwind(
			'$url'
		)
		.match({
			name: new RegExp(event.name)
		})
		.end()


	// .where({
	// 	name: new RegExp(event.name)
	// }).get()

	//返回数据给客户端
	return list
};
