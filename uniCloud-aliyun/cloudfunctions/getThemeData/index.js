'use strict';
const db = uniCloud.database();
const $ = db.command.aggregate
exports.main = async (event, context) => {
	
	const {id} = event;
	console.log(typeof id);
	let result = await db.collection('theme').aggregate()
		.lookup({
			from:'image',
			localField:'head_img_id',
			foreignField:'id',
			as:'headImg'
		})
		.lookup({
			from:'image',
			localField:'topic_img_id',
			foreignField:'id',
			as:'topicImg'
		})
		.addFields({
			headImgUrl:'$headImg.url',
			topicImgUrl:'$topicImg.url'
		})
		.project({
			headImg:0,
			topicImg:0,
			delete_time:0,
			update_time:0,
			head_img_id:0,
			topic_img_id:0
		})
		.match({
			id
		})
		.end()
	
	//返回数据给客户端
	return {
		code: 200,
		msg: '数据获取成功',
		data: result.data,
	}
};
