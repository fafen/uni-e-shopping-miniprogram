'use strict';
const db = uniCloud.database()
const $ = db.command.aggregate
exports.main = async (event, context) => {

	const Banner = db.collection('banner_item')
	let result = await Banner.aggregate()
		.lookup({
			from: 'image',
			localField: 'img_id',
			foreignField: 'id',
			as: 'images',
		})
		.replaceRoot({
			newRoot: $.mergeObjects([$.arrayElemAt(['$images', 0]), '$$ROOT'])
		})
		.project({
			images:0,
			delete_time: 0,
			update_time: 0,
			_id: 0,
			banner_id:0,
			img_id:0,
		})
		.end()


	//返回数据给客户端
	return {
		code: 200,
		msg: '数据获取成功',
		data: result.data,
	}
};
