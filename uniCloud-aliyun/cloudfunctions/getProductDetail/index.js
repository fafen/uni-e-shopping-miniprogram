'use strict';
const db = uniCloud.database();
const dbCmd = db.command
exports.main = async (event, context) => {
	
	const {id,arr} = event;
	let result = await db.collection('product')
	.aggregate()
	.lookup({
		from:"image",
		localField:"img_id",
		foreignField:"id",
		as:"imageUrl"
	})
	.unwind('$imageUrl')
	.addFields({
		url: '$imageUrl.url'
	})
	.project({
		imageUrl: false
	})
	.match({
		id: dbCmd.in(id?[id]:arr)
	})
	.end()
		
	//返回数据给客户端
	return {
		code: 200,
		msg: '数据获取成功',
		data: result.data,
	}
};
