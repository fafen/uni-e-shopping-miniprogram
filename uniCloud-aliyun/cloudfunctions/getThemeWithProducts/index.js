'use strict';
const db = uniCloud.database();
const $ = db.command.aggregate
const dbCmd = db.command
exports.main = async (event, context) => {

	const {
		id
	} = event;
	console.log(typeof id);
	let result = await db.collection('theme').aggregate()
		.lookup({
			from: 'theme_product',
			let:{
				id:'$id'
			},
			pipeline: $.pipeline().match(dbCmd.expr($.eq(['$theme_id','$$id'])))
			.project({
				theme_id:0,
				_id:0,
			})
			.done(),
			as: 'ThemeProducts'
		})
		.match({
			id,
		})
		.project({
			update_time: 0,
			delete_time: 0,
			head_img_id: 0,
			topic_img_id: 0,
		})
		.end()
		
	let data = result.data[0].ThemeProducts.map((item) => {
		return item.product_id
	})
	console.log(data);  //[1,2,3,5,6,15,33]
	
	let Products = await uniCloud.callFunction({
		name:"getProductDetail",
		data:{
			arr:data
		}
	})
	
	console.log(Products);


	//返回数据给客户端
	return {
		code: 200,
		msg: '数据获取成功',
		data: Products.result.data,
	}
};
