'use strict';
const uniID = require('uni-id')
exports.main = async (event, context) => {

	context.PLATFORM = 'mp-weixin'
	const uniIDIns = uniID.createInstance({
		context
	})

	const result = await uniIDIns.loginByWeixin({
		code: event.code
	})
	
	// uniID.createToken(Object CreateTokenParams)
	// const token = await uniID.createToken({
	// 	uid:res.uid,
	// 	openid: res.openid,
	// 	sessionKey: res.sessionKey,
	// 	unionid: res.unionid,
	// })


	//返回数据给客户端
	return result
};
