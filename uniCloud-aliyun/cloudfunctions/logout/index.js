'use strict';
const uniID = require('uni-id')
exports.main = async (event, context) => {
	
	const result = await uniID.logout(event.uniIdToken)
	
	//返回数据给客户端
	return result
};
